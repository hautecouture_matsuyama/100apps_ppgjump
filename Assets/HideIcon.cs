﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class HideIcon : MonoBehaviour {

    [SerializeField]
    private NendAdIcon titleIcon;


	void OnEnable()
    {
        titleIcon.Hide();
    }

    void OnDisable()
    {
        if(InterfaceManager.instance.currentScreen == ActiveScreen.Menu)
        titleIcon.Show();
    }
}
