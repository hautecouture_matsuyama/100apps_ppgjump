﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class ShowRanking : MonoBehaviour {

    [SerializeField]
    private GameObject RankingCanvas;

    [SerializeField]
    private NendAdIcon titleIcon;


    public void Show()
    {
        RankingCanvas.SetActive(true);
        titleIcon.Hide();
    }

    public void OtherButton()
    {
        Application.OpenURL("http://hautecouture.jp/hautecouture_services/game/minigame");
    }

}
