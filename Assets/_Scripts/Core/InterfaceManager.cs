﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using NendUnityPlugin.AD;

public enum TweenDirection { Left, Right, Up, Down }
public enum ActiveScreen { Menu, Upgrades, Options, Game, Death, Goals }

[System.Serializable]
public class TweenItem 
{
	public TweenItem () {}

	public RectTransform transform;
	public TweenDirection direction = TweenDirection.Right;

	[Space (10.0f)]

	public float padding;

	[Space (10.0f)]

	public Ease ease;
	public float speed;


	public Vector2 _to
	{
		get {
			Vector2 pos = transform.position;

			switch (direction)
			{
			case TweenDirection.Left:
				pos = new Vector2 (0 - (transform.rect.width / 2) - padding, transform.position.y);
				break;

			case TweenDirection.Right:
				pos = new Vector2 (Screen.width + (transform.rect.width / 2) + padding, transform.position.y);
				break;
			
			case TweenDirection.Up:
				pos = new Vector2 (transform.position.x, Screen.height + (transform.rect.height / 2) + padding);
				break;

			case TweenDirection.Down:
				pos = new Vector2 (transform.position.x, 0 - (transform.rect.height / 2) - padding);
				break;
			}

			return pos;
		}
	}

	private Vector2 _from;

	// -- to make sure we don't start a twee twice ... 
	private bool isTweening; 

	/// <summary>
	/// Tweens to the target position
	/// </summary>
	public void TweenTo ()
	{
		if (this.isTweening)
			return;

		this.isTweening = true;

		this._from = transform.position;

		this.transform.DOMove (_to, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
		});
	}

	/// <summary>
	/// Tweens to the target position with a callback once complete.
	/// </summary>
	public void TweenTo (System.Action OnComplete)
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;
		
		this._from = transform.position;
		
		this.transform.DOMove (_to, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
			OnComplete ();
		});
	}

	/// <summary>
	/// Tweens from out target position to it's original position
	/// </summary>
	public void TweenFrom ()
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;

		this.transform.DOMove (_from, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
		});
	}

	/// <summary>
	/// Tweens from out target position to it's original position
	/// </summary>
	public void TweenFrom (System.Action OnComplete)
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;
		
		this.transform.DOMove (_from, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
			OnComplete ();
		});
	}

	// -- Set Start position outside of screen? ... then use TweenFrom to bring it back on.
	public void StartFromEnd ()
	{
		this._from = transform.position;
		this.transform.position = this._to;
	}

	public void StartFromStart ()
	{
		this._from = transform.position;
		this.transform.position = this._from;
	}

	public void DebugDraw ()
	{
		//Debug.DrawLine (transform.position, _to);
	}
}

[System.Serializable]
public class TweenGroup
{
	public string name = "Tween Group";
	public bool _OnScreen;

	[Space (2.0f)]

	public TweenItem[] tweens;


	public void DebugDraw ()
	{
		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].DebugDraw ();
		}
	}

	public void PlayAllTo ()
	{
		_OnScreen = false;

		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].TweenTo ();
		}
	}

	public void PlayAllFrom ()
	{
		_OnScreen = true;

		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].TweenFrom ();
		}
	}
	
	// --
	public enum _StartingPosition {
		OnScreen, OffScreen
	}

	// -- wether the GUI should start on screen or not ... 
	public void SetStartingPosition (_StartingPosition startingPosition)
	{
		for (int i = 0; i < this.tweens.Length; i++)
		{
			if (startingPosition == _StartingPosition.OffScreen)
			{
				this._OnScreen = false;

				this.tweens [i].StartFromEnd ();
			}
			else
			{
				this._OnScreen = true;

				this.tweens [i].StartFromStart ();
			}
		}
	}
}

public static class TweenUtils
{
	public static TweenGroup GetGroup (TweenGroup[] groups, string name)
	{
		for (int i = 0; i < groups.Length; i++)
		{
			if (groups [i].name == name)
			{
				return groups [i];
			}
		}

		return null;
	}
}

public class InterfaceManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Tutrial;

    [SerializeField]
    private Image magnetBtn;

    [SerializeField]
    private Image multipleiBtn;

    [SerializeField]
    private Text myBestText;

    [SerializeField]
    private NendAdBanner rectangle;

    [SerializeField]
    private NendAdBanner banner;

    [SerializeField]
    private NendAdIcon overIcon;

    [SerializeField]
    private DFPBanner dfp;

    [SerializeField]
    private NendAdIcon titleIcon;


    // -- Singleton
    private static InterfaceManager _instance;
    public static InterfaceManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<InterfaceManager>();
            }

            return _instance;
        }
    }
    private Animator standAnimator;


    void Awake()
    {
        myBestText.text = PlayerPrefs.GetInt("_BestMeters").ToString() + "m";
    }
 

    //アップグレード画面のめんトリアニメーション
    //[SerializeField]
    //private Animator mentoriAnim;

    [SerializeField]
    private GameObject dontTouchPanel;


    // -- Manages all tweens for each screen.
    public TweenGroup[] tweenGroups;

    // -- Our current active screen
    public ActiveScreen currentScreen = ActiveScreen.Menu;


    //アップグレードゲージ最大数
    [SerializeField]
    private int MaxLevel = 5;


    void Start()
    {
        //standAnimator = mentoriAnim;


        // -- 
        DOTween.Init(true, true, LogBehaviour.ErrorsOnly);

        // -- 
        for (int i = 1; i < this.tweenGroups.Length; i++)
        {
            this.tweenGroups[i].SetStartingPosition(TweenGroup._StartingPosition.OffScreen);
        }

        this.tweenGroups[0].SetStartingPosition(TweenGroup._StartingPosition.OnScreen);


        //Initialize();

        //Debug.Log(Game.instance._Bank.GetBalance().ToString("N0"));

    }

    public void SelectGame()
    {
        // fade out menu BG ... 
        SwitchScreen(ActiveScreen.Game);
        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        BackButtonUtil.Instance.RemoveListenerAll();
        BackButtonUtil.Instance.AddListener(Game.instance.PauseGame);
        BackButtonUtil.Instance.AddListener(this.ViewGoals);

        // -- 
        ToggleMenuAlpha(0.0f, 0.3f);

        // -- Start the game
        if (!Game.instance._IsRunning)
        {
            Game.instance.StartGame();
        }
    }

    public void ToggleMenuAlpha(float _alpha, float _speed)
    {
        Image MenuBG = GameObject.Find("Menu_Background").GetComponent<Image>();

        MenuBG.DOFade(_alpha, _speed);
    }

    public void ToggleMenuAlpha(float _alpha, float _speed, System.Action CallBack)
    {
        Image MenuBG = GameObject.Find("Menu_Background").GetComponent<Image>();

        MenuBG.DOFade(_alpha, _speed).OnComplete(() =>
        {
            CallBack();
        });
    }

    public void SelectMenu()
    {
        if (Game.instance._IsRunning)
        {
            Game.instance.Die(false);
        }

        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.QuitGame);
        SwitchScreen(ActiveScreen.Menu);
    }

    public void SelectUpgrades()
    {
        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        BackButtonUtil.Instance.RemoveListenerAll();
        BackButtonUtil.Instance.AddListener(this.SelectMenu);
        SwitchScreen(ActiveScreen.Upgrades);

        UpdateUpgradesScreen();
    }

    public void SelectOptions()
    {
        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        BackButtonUtil.Instance.RemoveListenerAll();
        BackButtonUtil.Instance.AddListener(this.SelectMenu);

        SwitchScreen(ActiveScreen.Options);
    }

    public void ResumeGame()
    {
        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        BackButtonUtil.Instance.RemoveListenerAll();
        BackButtonUtil.Instance.AddListener(this.ViewGoals);
        BackButtonUtil.Instance.AddListener(Game.instance.PauseGame);

        SwitchScreen(ActiveScreen.Game);
    }

    public void ViewGoals()
    {
        BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        BackButtonUtil.Instance.RemoveListenerAll();
        BackButtonUtil.Instance.AddListener(this.SelectGame);
        BackButtonUtil.Instance.AddListener(Game.instance.ResumeGame);

        SwitchScreen(ActiveScreen.Goals);
    }

    //デバッグ用
    private void Initialize()
    {
        PlayerPrefs.SetInt("_JumpLevel", 0);
        PlayerPrefs.SetInt("_AtomMultiplier", 0);
        PlayerPrefs.SetInt("_MagnetLevel", 0);
    }

    //強化
    private void UpdateUpgradesScreen()
    {
        GameObject.Find("Upgrades_Screen").transform.FindChild("Panel").transform.FindChild("Bg").transform.FindChild("AtomBankText").GetComponent<Text>().text = Game.instance._Bank.GetBalance().ToString("N0");


        int multiplierLevel = PlayerPrefs.GetInt("_AtomMultiplier", 0);
        Game.instance.visuals.upgradeVisuals.items[1].levelIndicator.sprite = GetIndicatorSprite(multiplierLevel);
        Game.instance.visuals.upgradeVisuals.items[1].info.text = (multiplierLevel <= 4) ? UpgradeInfo.GetMultiplierCost(multiplierLevel).ToString("N0") : "MAX";
        Game.instance.visuals.upgradeVisuals.items[1].iconImage.sprite = Game.instance.visuals.upgradeVisuals.items[1].UpgradeIcons[multiplierLevel];

        int magnetLevel = PlayerPrefs.GetInt("_MagnetLevel", 0);
        Game.instance.visuals.upgradeVisuals.items[2].levelIndicator.sprite = GetIndicatorSprite(magnetLevel);
        Game.instance.visuals.upgradeVisuals.items[2].info.text = (magnetLevel <= 4) ? UpgradeInfo.GetMagnetCost(magnetLevel).ToString("N0") : "MAX";
        Game.instance.visuals.upgradeVisuals.items[2].iconImage.sprite = Game.instance.visuals.upgradeVisuals.items[2].UpgradeIcons[magnetLevel];
    }

    private Sprite GetIndicatorSprite(int level)
    {
        return Game.instance.visuals.upgradeVisuals.levelIndicators[level];
    }


    // -- upgrades ... 
    public void UpgradeJump()
    {
        int currentLevel = PlayerPrefs.GetInt("_JumpLevel", 0);

        if (currentLevel >= MaxLevel)
            return;

        // -- if we can afford it
        if (Game.instance._Bank.GetBalance() > UpgradeInfo.GetJumpSpeedCost(currentLevel))
        {
            Game.instance._Bank.Withdraw(UpgradeInfo.GetJumpSpeedCost(currentLevel));

            if (currentLevel < MaxLevel)
                currentLevel += 1;

            PlayerPrefs.SetInt("_JumpLevel", currentLevel);
        }

        this.UpdateUpgradesScreen();
    }

    
    private void MagnetGrayout()
    {
        //現在のレベルに必要なコスト
        int currentLevel = PlayerPrefs.GetInt("_MagnetLevel", 0);
        if (currentLevel >= MaxLevel)
            return;

        //持っているおでんの数がコストより多いか
        if (Game.instance._Bank.GetBalance() >= UpgradeInfo.GetMagnetCost(currentLevel))
        {
            magnetBtn.color = new Color(1f, 1f, 1f);
        }
        else
        {
            magnetBtn.color = new Color(0.5f, 0.5f, 0.5f);
        }
    }

    private void MultipleGrayout()
    {
        //現在のレベルに必要なコスト
        int currentLevel = PlayerPrefs.GetInt("_MagnetLevel", 0);
        if (currentLevel >= MaxLevel)
            return;

        //持っているおでんの数がコストより多いか
        if (Game.instance._Bank.GetBalance() >= UpgradeInfo.GetMultiplierCost(currentLevel))
        {
            multipleiBtn.color = new Color(1f, 1f, 1f);
        }
        else
        {
            multipleiBtn.color = new Color(0.5f, 0.5f, 0.5f);
        }
    }



    //マグネット強化
    public void UpgradeMagnet()
    {
        int currentLevel = PlayerPrefs.GetInt("_MagnetLevel", 0);

        // -- if we can afford it
        if (Game.instance._Bank.GetBalance() >= UpgradeInfo.GetMagnetCost(currentLevel))
        {
            //ボタン押せるように

            Game.instance._Bank.Withdraw(UpgradeInfo.GetMagnetCost(currentLevel));

            if (currentLevel < MaxLevel)
                currentLevel += 1;

            PlayerPrefs.SetInt("_MagnetLevel", currentLevel);

            MagnetGrayout();
            MultipleGrayout();

        }

        this.UpdateUpgradesScreen();
    }

    public void ToggleMusic()
    {
        bool mute = Camera.main.GetComponent<AudioSource>().volume > 0.0f ? true : false;

        if (mute)
        {
            Camera.main.GetComponent<AudioSource>().Pause();
        }
        else
        {
            Camera.main.GetComponent<AudioSource>().Play();
        }

        Camera.main.GetComponent<AudioSource>().volume = mute ? 0.0f : 1.0f;
    }

    public void ToggleSound()
    {
        float volume = Game.instance.GetComponent<AudioSource>().volume > 0.0f ? 0.0f : 0.2f;
        Game.instance.GetComponent<AudioSource>().volume = volume;
    }

    public void UpgradeMultiplier()
    {
        int currentLevel = PlayerPrefs.GetInt("_AtomMultiplier", 0);

        if (currentLevel >= MaxLevel)
            return;

        // -- if we can afford it
        if (Game.instance._Bank.GetBalance() > UpgradeInfo.GetMultiplierCost(currentLevel))
        {
            //ボタン押せるように

            Game.instance._Bank.Withdraw(UpgradeInfo.GetMultiplierCost(currentLevel));

            if (currentLevel < MaxLevel)
                currentLevel += 1;

            PlayerPrefs.SetInt("_AtomMultiplier", currentLevel);

            MagnetGrayout();
            MultipleGrayout();

            
        }

        this.UpdateUpgradesScreen();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        for (int i = 0; i < this.tweenGroups.Length; i++)
        {
            this.tweenGroups[i].DebugDraw();
        }
    }
#endif


    private int flg;

    void Active()
    {
        flg = PlayerPrefs.GetInt("FLG");

        if (flg != 1)
        {
            PlayerPrefs.SetInt("FLG", 1);

            Tutrial.SetActive(true);
            Game.instance.PauseGame();
        }

        else
        {
            return;
        }
    }

    IEnumerator ActiveTutrial()
    {
        flg = PlayerPrefs.GetInt("FLG");

        if (flg != 1)
        {
            PlayerPrefs.SetInt("FLG", 1);

            yield return new WaitForSeconds(0.5f);
            Game.instance.PauseGame();
            Tutrial.SetActive(true);
            
        }

        else
        {
            yield return new WaitForSeconds(0);
        }
    }


    public void SwitchScreen(ActiveScreen _screen)
    {
        StartCoroutine(DontPanel());

        // -- Make our current screen tween out 
        TweenGroup _OldGroup = TweenUtils.GetGroup(this.tweenGroups, currentScreen.ToString());
        _OldGroup.PlayAllTo();

        // -- 
        this.currentScreen = _screen;

        // -- 
        TweenGroup _NewGroup = TweenUtils.GetGroup(this.tweenGroups, currentScreen.ToString());
        _NewGroup.PlayAllFrom();

        Game game = new Game();


        


        switch (_screen)
        {
           
            case ActiveScreen.Game:
                //standAnimator.enabled = false;
                banner.Show();
                game.SwitchFlg();
                StartCoroutine(ActiveTutrial());
                //AdstirPlugin.instance.Shows();
                rectangle.Hide();
                overIcon.Hide();
                dfp.HideBanner();
                titleIcon.Hide();
                break;
            case ActiveScreen.Menu:
                //standAnimator.enabled = false;
                game.SwitchFlg();
                myBestText.text = PlayerPrefs.GetInt("_BestMeters").ToString()+"m";
                //AdstirPlugin.instance.Shows();
                rectangle.Hide();
                overIcon.Hide();
                banner.Show();
                dfp.ShowBanner();
                titleIcon.Show();
                break;
            case ActiveScreen.Goals:
                //standAnimator.enabled = false;
                game.SwitchFlg();
                //AdstirPlugin.instance.Shows();
                rectangle.Hide();
                overIcon.Hide();
                titleIcon.Hide();

                break;
            case ActiveScreen.Death:
                //standAnimator.enabled = false;
                //AdstirPlugin.instance.Shows();
                rectangle.Show();
                overIcon.Show();
                banner.Hide();
                dfp.HideBanner();
                titleIcon.Hide();
                break;
            //全面広告
            case ActiveScreen.Options:
                Debug.Log("OPYION");
                //standAnimator.enabled = false;
                game.SwitchFlg();
                rectangle.Hide();
                overIcon.Hide();
                dfp.HideBanner();
                titleIcon.Hide();
                //AdstirPlugin.instance.Shows();
                break;
            case ActiveScreen.Upgrades:
                Debug.Log("UPGRADE");
                rectangle.Hide();
                overIcon.Hide();
                MagnetGrayout();
                MultipleGrayout();
                game.SwitchFlg();
                dfp.HideBanner();
                titleIcon.Hide();
                banner.Show();
                //standAnimator.enabled = true;
                //AdstirPlugin.instance.HideAd();
                break;
        }

    }

    IEnumerator DontPanel()
    {
        dontTouchPanel.SetActive(true);
        yield return new WaitForSeconds(0.51f);
        dontTouchPanel.SetActive(false);
    }

}