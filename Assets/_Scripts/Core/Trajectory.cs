﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Trajectory
{
	private Vector3[] _points;
	private Planet start, end;

	public Trajectory (int iterations)
	{
		this._points = new Vector3 [iterations];

       
	}

	public List <Atom> CreateOnTrajectory (GameObject[] _prefabs, int cap)
	{
		List <Atom> tmp = new List <Atom> ();

		for (int i = cap; i < this._points.Length - cap; i++)
		{
			int randomAtom = (Random.value > 0.95f) ? 1 : 0;
			AtomType _type = randomAtom > 0.8f ? AtomType.Special : AtomType.Normal;
            //スコア（Special:Normal）
			int value = randomAtom > 0.8 ? 30 : 2;

			tmp.Add (new Atom (value, this._points [i], 0.35f, _prefabs [randomAtom], _type));


		}

		return tmp;
	}

	public void SetStart (Planet p)
	{
		this.start = p;
	}

	public void SetEnd (Planet p)
	{
		this.end = p;
	}

	public void AddPoint (int index, Vector3 p)
	{
		this._points [index] = p;
	}

	//public Vector2 HighestPoint ()
	//{
	//	return this._points.OrderBy (v => v.y).ToArray () [this._points.Length - 1];
	//}

	public Vector2 HighestPoint ()
	{
		float[] yValues = new float [this._points.Length];

		for (int i = 0; i < this._points.Length; i++)
		{
			yValues [i] = this._points [i].y;
		}

		float highestY = Mathf.Max (yValues);

		Vector2 midVector = (this.start.GetPosition () + this.end.GetPosition ()) / 2;
		midVector.y = highestY;

		return midVector;
	}

	public Planet GetStart ()
	{
		return this.start;
	}

	public Planet GetEnd ()
	{
		return this.end;
	}

	public void DrawTrajectory ()
	{
		for (int i = 1; i < this._points.Length; i++)
		{
			Gizmos.DrawLine (this._points [i - 1], this._points [i]);
		}

		Gizmos.DrawSphere (this.HighestPoint (), 0.25f);
	}
}
