﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveUtils
{
	// -- USAGE: PlayerPrefs.SetString ("KeyName", SaveManager.ObjectToStr <List <TYPE>> (serializableObj));
	public static string ObjectToStr <T> (T _saveMe)
	{
		BinaryFormatter _bin = new BinaryFormatter ();
		MemoryStream _mem = new MemoryStream ();
		_bin.Serialize (_mem, _saveMe);

		return System.Convert.ToBase64String (
			_mem.GetBuffer ()
		);
	}

	// -- USAGE: List <TYPE> _myserializableObj = SaveManager.StrToObject <List <TYPE>> (PlayerPrefs.GetString ("KeyName"));
	public static T StrToObject <T> (string _data) where T : class
	{
		if (!System.String.IsNullOrEmpty (_data)) 
		{
			BinaryFormatter _bin = new BinaryFormatter ();
			try 
			{
				MemoryStream _mem = new MemoryStream (System.Convert.FromBase64String (_data));
				
				T _obj = _bin.Deserialize (_mem) as T;
				
				return _obj;
			} 
			catch (UnityException ex) 
			{
				throw new UnityException (ex.Message);
			}
			
		} 
		else 
		{
			//throw new UnityException ("_data is null or empty");
			return null;
		}
	}

	// -- Dictionary to string
	public static string ToString (this Dictionary <string, string> d, string keyValueSeparator, string sequenceSeparator)
	{
		System.Text.StringBuilder sb = new System.Text.StringBuilder ();

		foreach (var x in d)
		{
			sb.Append (x.Key);
			sb.Append (keyValueSeparator);
			sb.Append (x.Value);
			sb.Append (sequenceSeparator);
		}

		return sb.ToString (0, sb.Length - 1);
	}
}