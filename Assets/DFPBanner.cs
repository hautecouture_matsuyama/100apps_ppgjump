﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using NendUnityPlugin.AD;

public class DFPBanner : MonoBehaviour {

    private BannerView bannerView;


    void Awake()
    {
        if(Random.value <= 0.33) StartCoroutine(Shows());

        GoogleAnalytics.StartTracking();
    }

    void Start()
    {
        RequestBanner();  
    }

    IEnumerator Shows()
    {
        yield return new WaitForSeconds(1.2f);
        NendAdInterstitial.Instance.Show();
    }

private void RequestBanner()
{
    #if UNITY_ANDROID
    string adUnitId = "/17192736/CN_GameAPP_12_header";
    #elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
    #else
        string adUnitId = "unexpected_platform";
    #endif

    // Create a 320x50 banner at the top of the screen.
    bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
    // Create an empty ad request.
    AdRequest request = new AdRequest.Builder().Build();
    // Load the banner with the request.
    bannerView.LoadAd(request);
}
    public void HideBanner()
{
    bannerView.Hide();
}

    public void ShowBanner()
    {
        bannerView.Show();
    }


}
