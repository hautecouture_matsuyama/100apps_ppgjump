﻿using UnityEngine;
using System.Collections;
using Kayac.Lobi.SDK;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class RankingManager : MonoBehaviour{


    //名前変更パネル
    [SerializeField]
    private GameObject nameChangePanel;

    //アタッチするGameObject名
    private string name = "RankingManager";

    //Lobideveloperにて表示されるID
    [SerializeField]
    private string rankingID = "lobimaster";

    public Text MyRankText;

    //ランキングのキャンバス
    public GameObject RankingCanvas;

    //ランキングのノード
    public GameObject Content;

    //ランキング情報保存用リスト
    private List<string> rankingList = new List<string>();


    //ランキングのノード用リスト
    private List<GameObject> gbList = new List<GameObject>();

    //ランキングのランク用のリスト（例：1位←ここ）
    //ランキングの桁数が変わった時に対応するためのリスト
    //五桁まで対応できる位置
    private List<GameObject> rankList = new List<GameObject>();

    //ランキングの名前用のリスト（例：たけし←ここ）
    private List<GameObject> nameList = new List<GameObject>();

    //ランキングのスコア用のリスト
    //五桁までのスコアに対応
    private List<GameObject> scoreList = new List<GameObject>();

    //ユーザーのアカウントID（自分のIDを検索するときに使用）
    private List<GameObject> IDList = new List<GameObject>();

    public static RankingManager instance;


    public enum RankingType{
        lobimaster
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        Initialize();
        RankingCanvas.active = false;
    }


    public void Initialize()
    {
        //ランキングのノードをすべて取得
        foreach (var n in Content.gameObject.GetChildren())
        {
            //各リストに初期値挿入
            switch (n.name)
            {
                case "rText":
                    rankList.Add(n);
                    break;
                case "nText":
                    nameList.Add(n);
                    break;
                case "sText":
                    scoreList.Add(n);
                    break;
                case "uidText":
                    IDList.Add(n);
                    break;
            }

        }

        //初期値代入
        for (int i = 0; i < rankList.Count; i++)
        {

            rankList[i].GetComponent<Text>().text = i + 1 + "位";
            nameList[i].GetComponent<Text>().text = "Player";
            scoreList[i].GetComponent<Text>().text = "" + 0;
        }

    }

    //ランキング送信処理
    public void SendRanking(int score)
    {
            LobiRankingAPIBridge.SendRanking(name, "SendRankingCallback", rankingID , score);
            GetMyRankingDate();   
    }

    //アカウント（名前）が登録済みがチェック
    //Awakeで呼び出すのが望ましい？
    public bool CheckAccount()
    {
        //登録済みか？
        if (LobiCoreBridge.IsSignedIn())
        {
            return true;
        }

        return false;

    }

    //ユーザー名登録処理
    public void SignUp(string username)
    {
        //登録できるか？
        if(!CanSignUp(username))
        {
            return;
        }

        //登録
        LobiCoreAPIBridge.SignupWithBaseName(name, "SignupWithBaseNameCallback", username);
        PlayerPrefs.SetString("USERNAME", username);
        int score = PlayerPrefs.GetInt("fastScore");
    }


    //サインアップのコールバック
    private void SignupWithBaseNameCallback(string message)
    {
        
        //GetRankingData();
    }


    private bool CanSignUp(string username)
    {
        //ユーザー名が空
        if(string.IsNullOrEmpty(username))
        {
            return false;
        }

        //ユーザー名を保存
        PlayerPrefs.SetString("USERNAME", username);
        
        //ネットワークに繋がっているか？
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            return false;
        }

        return true;
    }



    //ランキング表示処理（Lobiにつながる。今のところ使用する予定なし）
    public void ShowRanking()
    {
        LobiRankingBridge.PresentRanking();
    }


    private void SendRankingCallback(string message)
    {
        //ランキング情報を取得し、順位等を確認
        //sGetRankingData();
    }

    /// <summary>
    /// ランキングデータを取得する
    /// 
    /// GetRankingの引数
    /// 1.スクリプトをアタッチしているオブジェクトの名前
    /// 2.コールバック関数名
    /// 3.
    /// 4.表示したいランキングの期間(Today:今日、Week:今週 All:全体 LastWeek:先週 )
    /// 5.Top:先頭から表示、 Self:自分中心に表示
    /// 6.何位から表示するか
    /// 7.何位まで表示するか
    /// </summary>
    public void GetRankingData()
    {
            LobiRankingAPIBridge.GetRanking(
              name,
              "GetRankingCallback",
              rankingID,
              LobiRankingAPIBridge.RankingRange.Week,
              LobiRankingAPIBridge.RankingCursorOrigin.Top,
              1,
              100);
    }

    static int a = 0;

    public void GetMyRankingDate()
    {
        PlayerPrefs.SetString("MYR", "");

        LobiRankingAPIBridge.GetRanking(
              name,
              "GetMyRankingCallback",
              rankingID,
              LobiRankingAPIBridge.RankingRange.Week,
              LobiRankingAPIBridge.RankingCursorOrigin.Top,
              1,
              100);
     
    }

    private void GetMyRankingCallback(string message)
    {
        JSONObject json = new JSONObject(message).GetField("result");
        string Myrank = json.GetField("self_order").GetField("rank").str;
        PlayerPrefs.SetString("USERID", "" + json.GetField("self_order").GetField("uid").str.Replace("\"", ""));
        PlayerPrefs.SetString("MYR", Myrank);

        GetMyRankingDate2();

    }


    public void GetMyRankingDate2()
    {
        //PlayerPrefs.SetString("MYR", );

        LobiRankingAPIBridge.GetRanking(
              name,
              "GetMyRankingCallback2",
             rankingID,
              LobiRankingAPIBridge.RankingRange.Week,
              LobiRankingAPIBridge.RankingCursorOrigin.Top,
              1,
              100);

    }


    private void GetMyRankingCallback2(string message)
    {
        JSONObject json = new JSONObject(message).GetField("result");
        string Myrank = json.GetField("self_order").GetField("rank").str;
        PlayerPrefs.SetString("MYR", Myrank);
    }

    private void GetRankingCallback(string message)
    {
         //受け取ったmessageをJSONObject型に変えます
        JSONObject json = new JSONObject(message);
        //jsonで取得した名前格納用リスト
        List<string> jsonNameList = new List<string>();
        //jsonで取得したスコア格納用リスト
        List<string> jsonScoreList = new List<string>();

        //jsonで取得したユーザーIDのリスト
        List<string> jsonIDList = new List<string>();

        string username = PlayerPrefs.GetString("USERNAME");

        //JSONObjectのstatus_codeをInfoに入れます
        string Info = json.GetField("status_code").str;
 
        //成功時にはstatus_codeに0が入ります。
        if(Info == "0"){
            
            //ランキング一覧が入っているorderを取得します
            JSONObject DetaList = json.GetField("result").GetField("orders");
            JSONObject MYList = json.GetField("result").GetField("self_order");

            //PlayerPrefs.SetString("MYRANK", MyList.str);

            //json取得
            for(int t = 0; t < DetaList.Count; t++){
                jsonNameList.Add(""+DetaList[t].GetField("name"));
                jsonScoreList.Add("" + DetaList[t].GetField("score"));
                jsonIDList.Add("" + DetaList[t].GetField("uid"));

                /*自分の順位をPlayerprefsに格納する*/
                PlayerPrefs.SetString("MYRANK", "" + MYList.GetField("rank"));

            }

            string mrank = PlayerPrefs.GetString("MYRANK");

            MyRankText.text = mrank.Replace("\"", "") +"位";

      
            //ランキング用Canvasを表示
            RankingCanvas.active = true;

            //自分のユーザーID取得
            string myuserID = PlayerPrefs.GetString("USERID");

            //名前と
            for (int i = 0; i < jsonNameList.Count; i++)
            {
                nameList[i].GetComponent<Text>().text = jsonNameList[i].Replace("\"", "");
                scoreList[i].GetComponent<Text>().text = jsonScoreList[i].Replace("\"", "");
                IDList[i].GetComponent<Text>().text = jsonIDList[i].Replace("\"", "");
                if (IDList[i].GetComponent<Text>().text == myuserID)
                {
					scoreList[i].transform.parent.GetComponent<Image>().color = Color.white;
                }
            }
        }else{
            /*エラー時の処理*/
        }

    }

    //名前変更関数
    public void ChangeName(string userName)
    {
        if (!CanSignUp(userName))
        {
            return;
        }
        //コールバックを指定しないとiOSは動作しない
        LobiCoreAPIBridge.UpdateUserName(name, "ChangeNameCallback", userName);
        PlayerPrefs.SetString("USERNAME", userName);
    }

    private void ChangeNameCallback(string message)
    {
        JSONObject json = new JSONObject(message);
        string Info = json.GetField("status_code").str;


        //成功.
        if(Info == "0")
        {
            nameChangePanel.SetActive(false);
        }
       

    }



    public void DebugRanking()
    {
       
    }


}
