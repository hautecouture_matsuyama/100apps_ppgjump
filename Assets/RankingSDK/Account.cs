﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
using UnityEngine.UI;

public class Account : MonoBehaviour {

    public GameObject NameEditCanvas;

    [SerializeField]
    private NendAdIcon titleicon;

    [SerializeField]
    private GameObject dontPanel;

    [SerializeField]
    private Text dText;

    private int cheak;

    void Awake()
    {
        QualitySettings.vSyncCount = 0; // VSyncをOFFにする
        Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定

        titleicon.Hide();

        cheak = PlayerPrefs.GetInt("flg");

        //Editorか
        if (Debug.isDebugBuild)
            return;
        //ネット接続されているか
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return;
        //アカウントは存在するか
        if (cheak == 1)
            return;

        StartCoroutine(ShowNameEdit());

    }


    private IEnumerator ShowNameEdit()
    {
        dontPanel.SetActive(false);

        yield return new WaitForSeconds(0.5f);
        PlayerPrefs.SetInt("flg", 1);
        NameEditCanvas.SetActive(true);

    }

}
