﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using NendUnityPlugin.AD;


public class EditOKButton : MonoBehaviour {

    public InputField nameText;
    public GameObject NameEditCanvas;
    public GameObject ErrPanel;

    [SerializeField]
    private NendAdIcon titleIcon;


    void Start()
    {
        SetDefaultName();
    }


    public void GetName()
    {
        string strAfter = "";

        //入力されているか？
        if (nameText.text == string.Empty)
            return;

        if (nameText.text.Length > 5) strAfter = nameText.text.Remove(5);

        else strAfter = nameText.text;

        //ユーザー名登録
        RankingManager.instance.ChangeName(strAfter);
        titleIcon.Show();
        NameEditCanvas.active = false;
    }

    void SetDefaultName()
    {
        TextAsset nameTextFile = Resources.Load("text/defaultname", typeof(TextAsset)) as TextAsset;
        string[] nameList = nameTextFile.text.Split("\n"[0]);
        Debug.Log("NAMEEEEEEEEEEEEEE");
        int idx = Random.RandomRange(0,nameList.Length);
        nameText.text = nameList[idx].Replace("\r","");
        RankingManager.instance.SignUp(nameText.text);

    }


    public static bool IsKana(string target)
    {
        foreach (var chara in target)
        {
            var charaData = Encoding.UTF8.GetBytes(chara.ToString());

            if (charaData.Length != 3)
            {
                return false;
            }

            var charaInt = (charaData[0] << 16) + (charaData[1] << 8) + charaData[2];

            if (charaInt < 0xe38181 || charaInt > 0xe38293)
            {
                return false;
            }
        }

        return true;
    }
}
